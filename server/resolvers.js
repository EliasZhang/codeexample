module.exports = {
    Query: {
        machines: (_, __, { dataSources }) =>
            dataSources.MachinesAPI.getMachines(),
        machine: (_, { id }, { dataSources }) =>
            dataSources.MachinesAPI.getMachineById({ machineId: id }),
        sensorsData: (_, __, { dataSources }) =>
            dataSources.SensorsDataAPI.getAllData(),
        sensorData: (_, { sensorId, from, to }, { dataSources }) =>
            dataSources.SensorsDataAPI.getDataById({ sensorId: sensorId, from: from, to: to }),
    },
};