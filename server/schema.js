const { gql } = require('apollo-server');

const typeDefs = gql`
    type Query {
        machines: [Machine!]
        machine(id: String!): Machine
        sensorData(sensorId: [String], from: Int, to: Int): [SensorDataPoint]
        sensorsData: [SensorDataPoint]
    }

    type Machine {
        id: ID!
        name: String!
        detail: String
        lastKnownPosition: String
        sensors: [Sensor!]
    }
    
    type Sensor {
        name: String!
        sensorId: ID!
    }
    
    type SensorDataPoint {
        name: String!
        sensorId: ID!
        timestamp: Int!,
        value: Float!
    }
`;

module.exports = typeDefs;