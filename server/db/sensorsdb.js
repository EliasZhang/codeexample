const sensorsdb = [
    {
        name: "sensor 1",
        sensorId: "s-1",
        timestamp: 1582021371,
        value: 100
    },
    {
        name: "sensor 1",
        sensorId: "s-1",
        timestamp: 1579451703,
        value: 150
    },
    {
        name: "sensor 1",
        sensorId: "s-1",
        timestamp: 1379451703,
        value: 10
    },
    {
        name: "sensor 1",
        sensorId: "s-1",
        timestamp: 1279451703,
        value: 88
    },
    {
        name: "sensor 1",
        sensorId: "s-1",
        timestamp: 1479451703,
        value: 90
    },
    {
        name: "sensor 2-1",
        sensorId: "s-2-1",
        timestamp: 1582021371,
        value: 200
    },
    {
        name: "sensor 2-2",
        sensorId: "s-2-2",
        timestamp: 1579451703,
        value: 350
    },
    {
        name: "sensor 2-2",
        sensorId: "s-2-2",
        timestamp: 1582021371,
        value: 300
    },
    {
        name: "sensor 3-1",
        sensorId: "s-3-1",
        timestamp: 1582021371,
        value: 10
    },
    {
        name: "sensor 3-1",
        sensorId: "s-3-1",
        timestamp: 1582121371,
        value: 100
    },
    {
        name: "sensor 3-2",
        sensorId: "s-3-2",
        timestamp: 1582021371,
        value: 50
    },
    {
        name: "sensor 3-2",
        sensorId: "s-3-1",
        timestamp: 1582121371,
        value: 60
    },
    {
        name: "sensor 3-3",
        sensorId: "s-3-1",
        timestamp: 1582021371,
        value: 20
    },
    {
        name: "sensor 3-3",
        sensorId: "s-3-1",
        timestamp: 1582121371,
        value: 50
    },
];

module.exports = sensorsdb;