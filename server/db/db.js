const db = [
    {
        id: "1",
        name: "Machine a",
        lastKnownPosition: "USA",
        detail: "detail of machine c",
        sensors: [
            {
                name: "sensor 1",
                sensorId: "s-1"
            }
        ]
    },
    {
        id: "2",
        name: "Machine b",
        lastKnownPosition: "Germany",
        detail: "detail of machine b",
        sensors: [
            {
                name: "sensor 2-1",
                sensorId: "s-2-1"
            },
            {
                name: "sensor 2-2",
                sensorId: "s-2-2"
            }
        ]
    },
    {
        id: "3",
        name: "Machine c",
        lastKnownPosition: "Poland",
        detail: "detail of machine c",
        sensors: [
            {
                name: "sensor 3-1",
                sensorId: "s-3-1"
            },
            {
                name: "sensor 3-2",
                sensorId: "s-3-2"
            },
            {
                name: "sensor 3-2",
                sensorId: "s-3-2"
            }
        ]
    },
    {
        id: "4",
        name: "Machine D",
        lastKnownPosition: "Colombia",
        detail: "detail of machine D",
        sensors: []
    }
];

module.exports = db;