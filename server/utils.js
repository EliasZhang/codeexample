const db = require("./db/db");
const sensorDb = require("./db/sensorsdb");

module.exports = {
    createStore:() => {
        const machines = db.map(machine => {
            return machine
        });
        return { machines }
    },
    createSensorStore:() => {
        const sensors = sensorDb.map(sensorData => {
            return sensorData
        });
        return { sensors }
    }
};