const { DataSource } = require("apollo-datasource");

class SensorsDataAPI extends DataSource {
    constructor({ sensorDataStore }) {
        super();
        this.sensorDataStore = sensorDataStore;
    }

    async getAllData() {
        return this.sensorDataStore.sensors;
    }

    async getDataById({ sensorId, from, to }) {
        let sensorsData = [];
        for (let i = 0; i < sensorId.length; i++) {
            let sensorData = this.sensorDataStore.sensors.filter(sensor =>
            {
                if (from === null && to === null) {
                    return sensor.sensorId === sensorId[i]
                } else if (from === null) {
                    return sensor.sensorId === sensorId[i] && sensor.timestamp <= to
                } else if (to === null) {
                    return sensor.sensorId === sensorId[i] && sensor.timestamp >= from
                } else {
                    return sensor.sensorId === sensorId[i] && sensor.timestamp >= from && sensor.timestamp <= to
                }
            });
            sensorsData = sensorsData.concat(sensorData);
        }
        return sensorsData;
    }

}

module.exports = SensorsDataAPI;