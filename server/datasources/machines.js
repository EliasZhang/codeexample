const { DataSource } = require("apollo-datasource");

class MachinesAPI extends DataSource {
    constructor({ store }) {
        super();
        this.store = store
    }

    async getMachines() {
        return this.store.machines
    }

    async getMachineById({ machineId }) {
        const machine = this.store.machines.find(machine => machine.id === machineId);
        return machine
    }
}

module.exports = MachinesAPI;