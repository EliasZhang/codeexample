const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const { createStore, createSensorStore } = require("./utils");

const MachinesAPI = require('./datasources/machines'),
    SensorsDataAPI = require('./datasources/sensorsData');

const store = createStore(),
    sensorDataStore = createSensorStore();

const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => {
        return {
            MachinesAPI: new MachinesAPI({ store }),
            SensorsDataAPI: new SensorsDataAPI({ sensorDataStore })
        }
    }
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});