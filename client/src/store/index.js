import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedSensorId: [],
    fromTime: null,
    toTime: null
  },
  mutations: {
    setSelectedSensorId (state, newIdList) {
      state.selectedSensorId = newIdList
    },
    setFromTime (state, newFromTime) {
      state.fromTime = newFromTime
    },
    setToTime (state, newToTime) {
      state.toTime = newToTime
    }
  },
  actions: {
  },
  modules: {
  }
})
