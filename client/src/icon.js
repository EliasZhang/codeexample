import { library } from '@fortawesome/fontawesome-svg-core';
import Vue from 'vue'

// internal icons
import { faCalendar, faCheck, faSignal, faTimes, faChartPie, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
  faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

function addIcon(){
  library.add(faCalendar, faCheck,faSignal, faTimes, faChartPie, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
      faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
      faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload);

  Vue.component('vue-fontawesome', FontAwesomeIcon);
}

export { addIcon };