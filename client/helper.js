function getAllLabel(data) {
    let label = [];
    data.forEach(each=>{
        if(!label.includes(each.timestamp)){
            label.push(each.timestamp);
        }
    });
    // sort time for ascending sort
    label = label.sort((a, b) => a - b);
    return label
}

function getInitialDataset(data, numberOfTimestamp) {
    let initialDataset = [];
    let DatasetLabel = [];
    data.forEach(each => {
        if(!DatasetLabel.includes(each.name)){
            DatasetLabel.push(each.name);
        }
    });

    DatasetLabel.forEach(eachLabel => {
        initialDataset.push({ label: eachLabel,
            data: new Array(numberOfTimestamp).fill(null) })
        }
    );
    return initialDataset
}

function setDataIntoInitialDataset(initialDataset, data, allTimestamp) {
    let finalDataset = initialDataset;
    for (let i = 0; i < data.length; i++) {
        let indexOfDataset = initialDataset.findIndex(eachDataset => {
            return eachDataset.label === data[i].name;
        });

        let indexOfTimestamp = allTimestamp.findIndex(timestamp => {
            return timestamp === data[i].timestamp
        });
        finalDataset[indexOfDataset].data[indexOfTimestamp] = data[i].value;
    }
    return finalDataset
}

export { getAllLabel, getInitialDataset, setDataIntoInitialDataset }