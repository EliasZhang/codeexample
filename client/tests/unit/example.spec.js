import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import GraphButton from "../../src/components/buttom/GraphButton";

describe('GraphButton.vue', () => {
  it('The button can routed to chart page', () => {
    const msg = 'new message';
    const wrapper = shallowMount(GraphButton);
    let button = wrapper.find('.test')
    expect(button.props('to')).toBe('/expected/path')
  })


});
